<?php

namespace Drupal\connectorg_employee_engagement\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Star entities.
 *
 * @ingroup connectorg_employee_engagement
 */
class StarEntityDeleteForm extends ContentEntityDeleteForm {


}
