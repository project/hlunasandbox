<?php

namespace Drupal\connectorg_office\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Office entities.
 *
 * @ingroup connectorg_office
 */
class OfficeEntityDeleteForm extends ContentEntityDeleteForm {


}
